resource "yandex_lb_target_group" "kubernetus" {
  name      = "kubernetus"

    dynamic "target" {
    for_each = [for node in yandex_compute_instance.nodes : {
      address   = node.network_interface.0.ip_address
      subnet_id = node.network_interface.0.subnet_id
    }]

    content {
      subnet_id = target.value.subnet_id
      address   = target.value.address
    }
  }
}
