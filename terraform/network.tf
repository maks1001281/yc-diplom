resource "yandex_vpc_network" "localcloud" {
  name = "localcloud"
}

resource "yandex_vpc_subnet" "localsubnet" {
  count          = 3
  name           = "localsubnet-${var.zones[count.index]}"
  zone           = var.zones[count.index]
  network_id     = yandex_vpc_network.localcloud.id
  v4_cidr_blocks = [var.cidr[count.index]]
}
