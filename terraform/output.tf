output "worker_node" {
  value = {
    for node in yandex_compute_instance.nodes:
        node.hostname => node.network_interface.0.nat_ip_address
  }
}

output "master" {
  value = yandex_compute_instance.master.network_interface.0.nat_ip_address
}



