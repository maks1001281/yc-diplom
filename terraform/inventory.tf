resource "local_file" "kubespray" {
  content  =<<EOF
[all]
 node0 ansible_host=${yandex_compute_instance.master.network_interface.0.nat_ip_address} ansible_user=ubuntu  # ip=10.3.0.1 etcd_member_name=etcd1
 node1 ansible_host=${yandex_compute_instance.nodes.0.network_interface.0.nat_ip_address} ansible_user=ubuntu  # ip=10.3.0.2 etcd_member_name=etcd2
 node2 ansible_host=${yandex_compute_instance.nodes.1.network_interface.0.nat_ip_address} ansible_user=ubuntu  # ip=10.3.0.3 etcd_member_name=etcd3
 node3 ansible_host=${yandex_compute_instance.nodes.2.network_interface.0.nat_ip_address}  ansible_user=ubuntu  # ip=10.3.0.4 etcd_member_name=etcd4

# ## configure a bastion host if your nodes are not directly reachable
# [bastion]
# bastion ansible_host=x.x.x.x ansible_user=some_user

[kube_control_plane]
 node0
[etcd]
 node0

[kube_node]
 node1
 node2
 node3
[calico_rr]

[k8s_cluster:children]
kube_control_plane
kube_node
calico_rr

 EOF

  filename = "../kubespray/inventory/sample/inventory.ini"
}

resource "local_file" "ansiblemaster" {
  content  =<<EOF
${yandex_compute_instance.master.network_interface.0.nat_ip_address} ansible_user=ubuntu
EOF
  filename = "../ansible/hosts.txt"
}
