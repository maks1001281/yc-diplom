resource "yandex_lb_network_load_balancer" "kubernetus" {
  name = "kubernetus"
  deletion_protection = "false"
  listener {
    name        = "app"
    port        = 80
    target_port = 30001
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  listener {
    name        = "grafana"
    port        = 3000
    target_port = 30002
    protocol    = "tcp"
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.kubernetus.id
    healthcheck {
      name                = "http"
      interval            = 2
      timeout             = 1
      unhealthy_threshold = 2
      healthy_threshold   = 2
      http_options {
        port = 30001
        path = "/"
      }
    }
  }
}

