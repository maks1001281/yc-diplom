resource "null_resource" "timewait" {
  provisioner "local-exec" {
    command = "sleep 60"
  }
depends_on = [
   yandex_lb_target_group.kubernetus
  ]
}

resource "null_resource" "kubespray" {
  provisioner "local-exec" {
    command = "ansible-playbook -i ../kubespray/inventory/sample/inventory.ini ../kubespray/cluster.yml -become"
  }


depends_on = [
   null_resource.timewait
  ]
}

#resource "null_resource" "gitlab" {
#  provisioner "local-exec" {
#    command = "ansible-playbook -i ../ansible/hosts.txt ../ansible/main.yaml -become"
#  }
#
#
#depends_on = [
#   null_resource.kubespray
#  ]
#}


