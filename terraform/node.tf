resource "yandex_compute_instance" "nodes" {
  count    = 3
  name     = "node${count.index + 1}"
  hostname = "node${count.index + 1}"
  zone     = var.zones[count.index]
  platform_id = "standard-v1"
  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id    = "fd8bkgba66kkf9eenpkb"
      type        = "network-nvme"
      size        = "30"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.localsubnet[count.index].id
    nat       = true
    ipv6      = false
  }

  scheduling_policy {
    preemptible = true
  }

   metadata = {
    ssh-keys = "centos:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1WhM79zJvnP4gIlKFQFcxl+7/bycjrGFT28qEOPXIpkw0DwNPrwaJYhHAahFRa4m9ZxZd3Fcv8NxkqQIVPxfWv9EAAzYsM0w5D1r7KseE3OnD7p0bfRGTQHxmmMSCeAWQv60wVRsWMQAwP/tDYxerLgxTEL2H9q3sbbgBcgQChOUBZardCl3FS4yfW4MfID09sSQU3RpavLf7WFzbC1ZKxKcd0881p0v8XnFx6onMgJba5+SL3VuEP+qfN4InQ1AMIorrplFiU3A6gTdCXJiimcBw9nSYg1x29bTNDbULDhU7j9//qRAkrO7nwXea1bl/AMt7cKdonBKJi+r/C7hF root@deploy"
  }
}

