

### Создание облачной инфраструктуры
 ### Использовал Terraform и [код](https://gitlab.com/maks1001281/yc-diplom/-/tree/main/terraform?ref_type=heads) на своей виртуальной машине
 ### После успешного выполнения кода видим готовую инфраструктуру с 4 ВМ, сетевым балансировщиком, kubernetus кластером, и VPC в разных зонах доступности, `terraform destroy -auto-approve` выполнятеся без ошибок, вместе с кодом генерируется inventory для kubespray и playbook.
 ![Alt text](image/terraform.PNG)
 ![Alt text](image/vpc.PNG)  ![Alt text](image/lb.PNG)
 
 
### Создание Kubernetes кластера
 ### Для поднятия кластера использовал [Kubespray](https://github.com/kubernetes-sigs/kubespray), одна master нода в качестве control plane и 3 worker node, [Конфигурация](https://gitlab.com/maks1001281/yc-diplom/-/tree/main/kubespray?ref_type=heads)
 ### Успешно выполненный  playbook и проверка функционирования кластера
 ![Alt text](image/cluster.PNG) 
 ![Alt text](image/kubectl.PNG)
 ### Был написан [Playbook](https://gitlab.com/maks1001281/yc-diplom/-/blob/main/ansible/main.yaml) который устанавливает и настраивает базовые системный утилиты, helm, gitlab-runners, kube-prometeus, для запуска playbook необходимо выполнить команду `ansible-playbook -i hosts.txt main.yaml -become`.
 ![Alt text](image/ansible.PNG)
 
 
 ### Создание тестового приложения
  ### За тестовое приложение был взят статический общедоступный шаблон сайта который я шаблонизировал в Helm [config](https://gitlab.com/maks1001281/yc-diplom/-/tree/main/helm/site)
  ### Dоcker [образ](dockerfile) был сознан на моем локальном docker VM и отправлен в [gitlab](https://gitlab.com/maks1001281/yc-diplom/container_registry)  


### Подготовка cистемы мониторинга и деплой приложения
 ### Deploy системы мониторинга был применен на предыдущем шаге с помощью playbook, для получения доступа к grafana и приложению необходимо склонировать git репозиторий на control plane  и установить helm Chart  командой `helm install site/ --generate-name` и вставить имя чарта в gitlab-ci.yml для дальнейшего выполнения pipeline и версионирования helm, Внимание! после применения Chart интерфейс grafana будет доступен по порту 3000, необходимо будет сразу изменить стандартный пароль на свой.
 ### После применения Helm chart [Приложение](http://158.160.99.207) и [grafana](http://158.160.99.207:3000) стали доступны
 ![Alt text](image/site.PNG)
 ![Alt text](image/grafana.PNG)
 
 
 ### Установка и настройка CI/CD
  ### В качестве CI\CD был выбран [gitlab](https://gitlab.com/maks1001281/yc-diplom)
  ### При любом коммите c тегом в репозиторий с тестовым приложением происходит сборка и отправка в регистр Docker образа а так же deploy приложения в kubernetus
  ### Делаем commit и вешаем тег для приложения, смотрим успешно выполненный pipeline и новую версию приложения
   ![Alt text](image/job.PNG)
   ![Alt text](image/mdoshuk.PNG)